import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from './chat.service';
import { Observable } from 'rxjs';
import { Message } from '../../../shared/types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = new FormControl('');
  message = new FormControl('');
  allMessages: Observable<Message[]>;
  constructor(private chat: ChatService) {
    this.allMessages = chat.messages;
  }

  sendChat() {
    this.chat.sendChat({
      name: this.name.value,
      contents: this.message.value
    });
    this.message.reset();
  }
}

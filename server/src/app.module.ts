import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EmployeesController } from './employees/employees.controller';
import { HrService } from './hr/hr.service';
import { ChatGateway } from './chat/chat.gateway';

@Module({
  imports: [],
  controllers: [AppController, EmployeesController],
  providers: [AppService, HrService, ChatGateway],
})
export class AppModule {}

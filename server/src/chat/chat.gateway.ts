import { WebSocketGateway, SubscribeMessage, WebSocketServer } from '@nestjs/websockets';
import { Client, Server } from 'socket.io';

import { Message } from '../../../shared/types';

@WebSocketGateway()
export class ChatGateway {
  @WebSocketServer() server: Server;
  private messages: Message[] = [];

  @SubscribeMessage('chat')
  addChatMessage(client: Client, message: Message) {
    this.messages.push(message);
    this.server.emit('chat', this.messages);
    return 'Message received from ' + message.name;
  }

}

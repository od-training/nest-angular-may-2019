import { Injectable } from '@nestjs/common';
import axios from 'axios';

const baseUrl = 'http://api.angularbootcamp.com/';
const empPath = 'employees';
const todoPath = 'todos';

@Injectable()
export class HrService {
  async count(path: string): Promise<number> {
    const response = await axios.get<any[]>(
      baseUrl + path,
    );
    return response.data.length;
  }

  async employeeCount(): Promise<number> {
    return this.count(empPath);
  }

  async todoCount(): Promise<number> {
    return this.count(todoPath);
  }
}


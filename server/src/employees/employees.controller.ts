import { Controller, Get } from '@nestjs/common';
import { HrService } from '../hr/hr.service';

@Controller('employees')
export class EmployeesController {
  constructor(private hr: HrService) { }

  @Get()
  async getEmployees() {
    const employeePromise = this.hr.employeeCount();
    const todoPromise = this.hr.todoCount();
    const employeeCount = await employeePromise;
    let todoCount: number | string = 'UNKNOWN';
    try {
      todoCount = await todoPromise;
    } catch (error) {
      console.log(
        'proceeding in spite of TODO API failure'
      );
    }
    return {
      employeeCount,
      todoCount,
    };
  }
}
